package com.antoonvereecken.sandboxreactive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Person {

    private String firstName;
    private String lastName;

    public String toWelcomeString() {
        return "Welcome " + firstName + " " + lastName + "!";
    }


}
