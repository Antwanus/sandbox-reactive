package com.antoonvereecken.sandboxreactive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonCommand {

    private String firstName;
    private String lastName;

    public PersonCommand(Person p) {
        this.firstName = p.getFirstName();
        this.lastName = p.getLastName();
    }

    public String toWelcomeString() {
        return "Welcome " + firstName + " " + lastName + "!";
    }


}
