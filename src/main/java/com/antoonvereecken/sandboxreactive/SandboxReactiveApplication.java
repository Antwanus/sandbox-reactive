package com.antoonvereecken.sandboxreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SandboxReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxReactiveApplication.class, args);
	}

}
