package com.antoonvereecken.sandboxreactive;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class ReactiveExampleTest {

    Person p1 = new Person("Bertha", "Big");
    Person p2 = new Person("John", "Little");
    Person p3 = new Person("Harry", "Dirty");
    Person p4 = new Person("Winnie", "Pooh");
    Person p5 = new Person("Elon", "Musk");

    @Test
    void monoTests() {
        Mono<Person> personMono = Mono.just(p1);        // create new mono from one Person
        Person p = personMono.block();                  // get person object from publisher
        log.info(p.toWelcomeString());                  // output name from publisher
    }
    @Test
    void monoTransform() {
        Mono<Person> personMono = Mono.just(p2);        // create new mono from one Person
        PersonCommand personCommand = personMono.map(   // transform the mono<Person> to PersonCommand
                person -> new PersonCommand(person)
        ).block();
        log.info(personCommand.toWelcomeString());
    }
    @Test
    void monoFilterUnknownThrowsNullPointerException() {
        Mono<Person> personMono = Mono.just(p3);
        assertThrows(NullPointerException.class, () -> {
            Person p = personMono.filter(
                    (Person person) -> person.getFirstName().equalsIgnoreCase("foo")
            ).block();
            log.info(p.toWelcomeString());
        });
    }
    @Test
    void fluxTest() {
        Flux<Person> peopleFlux = Flux.just(p1, p2, p3, p4, p5);
        peopleFlux.filter(
                (Person p) -> p.getFirstName().equalsIgnoreCase("John")
        ).subscribe(
                (Person person) -> log.info(person.toWelcomeString()));
    }
    @Test
    void fluxFilterTest() {
        Flux<Person> peopleFlux = Flux.just(p2, p3, p4, p5);
        peopleFlux.subscribe(e -> log.info(e.toWelcomeString()));
    }
    @Test
    void fluxDelayNoOutputTest() {
        Flux<Person> personFlux = Flux.just(p3, p4, p5);
        personFlux.delayElements(Duration.ofSeconds(1))
                .subscribe(person -> log.info(person.toWelcomeString()));
    }
    @Test
    void fluxDelayWithOutputTest() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Flux<Person> personFlux = Flux.just(p3, p4, p5);

        personFlux.delayElements(Duration.ofSeconds(1))
                .doOnComplete(countDownLatch::countDown)
                .subscribe(person -> log.info(person.toWelcomeString()));

        countDownLatch.await();
    }
    @Test
    void fluxFilterAndDelayWithOutputTest() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Flux<Person> personFlux = Flux.just(p1, p2, p3, p4, p5);

        personFlux.delayElements(Duration.ofSeconds(1))
                .filter(
                        person -> person.getFirstName().contains("a")
                )
                .doOnComplete(countDownLatch::countDown)
                .subscribe(person -> log.info(person.toWelcomeString()));

        countDownLatch.await();
    }
}
